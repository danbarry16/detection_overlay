import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
from vision_msgs.msg import Detection2DArray
from std_msgs.msg import String


class DetectionOverlay(Node):

    def __init__(self):
        super().__init__('detection_overlay')
        # Subscribe to raw camera
        self.sub_image = self.create_subscription(
            Image,
            '/image_raw',
            self.image_callback,
            10
        )
        # Subscribe to detections
        self.sub_detect = self.create_subscription(
            Detection2DArray,
            '/detector_node/detections',
            self.detect_callback,
            10
        )
        # Publisher
        self.img_msg = None
        self.det_msg = None
        self.publisher_ = self.create_publisher(Image, 'overlay', 10)
        timer_period = 0.5  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)
        self.i = 0

    def timer_callback(self) :
        if self.img_msg != None :
            # Draw overlay
            if self.det_msg != None :
                # Check detections
                for det in self.det_msg.detections :
                    # Calculate bounding box
                    x1 = det.bbox.center.position.x - det.bbox.size_x
                    y1 = det.bbox.center.position.y - det.bbox.size_y
                    x2 = det.bbox.center.position.x + det.bbox.size_x
                    y2 = det.bbox.center.position.y + det.bbox.size_y
                    # Error checking for out of bounds
                    x1 = min(max(0, int(x1)), self.img_msg.width - 1)
                    y1 = min(max(0, int(y1)), self.img_msg.height - 1)
                    x2 = min(max(0, int(x2)), self.img_msg.width - 1)
                    y2 = min(max(0, int(y2)), self.img_msg.height - 1)
                    # Top & bottom
                    for x in range(x1, x2) :
                        self.img_msg.data[(x + (self.img_msg.width * y1)) * 3] = 0xFF
                        self.img_msg.data[(x + (self.img_msg.width * y2)) * 3] = 0xFF
                    # Left & Right
                    for y in range(y1, y2) :
                        self.img_msg.data[(x1 + (self.img_msg.width * y)) * 3] = 0xFF
                        self.img_msg.data[(x2 + (self.img_msg.width * y)) * 3] = 0xFF
                self.det_msg = None
            self.publisher_.publish(self.img_msg)
            self.get_logger().info('Publishing: "%i"' % self.i)
            self.i += 1

    def image_callback(self, msg) :
        self.get_logger().info('Got image: "%i"' % msg.step)
        self.img_msg = msg
        return

    def detect_callback(self, msg) :
        self.get_logger().info('Got detection: "%i"' % len(msg.detections))
        self.det_msg = msg
        return


def main(args=None):
    rclpy.init(args=args)

    node = DetectionOverlay()

    rclpy.spin(node)

    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
